package models

type Mail struct {
	To      []string `json:"to" validate:"required"`
	Cc      []Cc     `json:"cc"`
	From    string   `json:"from"`
	Subject string   `json:"subject" validate:"required"`
	Message string   `json:"message" validate:"required"`
}

type Cc struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}
