package routers

import (
	"fmt"
	"hermes/config"
	"hermes/controller"
	"net/http"

	"github.com/gin-gonic/gin"
)

type router struct {
	cfg *config.Env
}

func NewRouter(cfg *config.Env) *router {
	return &router{
		cfg: cfg,
	}
}

func (r *router) Setup() {
	app := gin.Default()

	mailController := controller.NewMailController(r.cfg)
	app.POST("/send-email", mailController.SendMail)

	app.GET("/health-check", func(c *gin.Context) {
		c.JSONP(http.StatusOK, gin.H{
			"status": true,
			"code":   http.StatusOK,
		})
	})

	addrs := fmt.Sprintf("%v:%v", r.cfg.AppHost, r.cfg.AppPort)
	app.Run(addrs)
}
