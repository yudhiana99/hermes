module hermes

go 1.13

require (
	github.com/TwinProduction/go-color v1.0.0
	github.com/caarlos0/env/v6 v6.5.0
	github.com/gin-gonic/gin v1.7.1
	github.com/go-playground/validator/v10 v10.11.1
	github.com/joho/godotenv v1.3.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
