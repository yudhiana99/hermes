package config

import (
	b64 "encoding/base64"
	"fmt"
	"os"

	"github.com/TwinProduction/go-color"
)

const label = "CiBfICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKfCB8X18gICBfX18gXyBfXyBfIF9fIF9fXyAgIF9fXyAgX19fIAp8ICdfIFwgLyBfIFwgJ19ffCAnXyBgIF8gXCAvIF8gXC8gX198CnwgfCB8IHwgIF9fLyB8ICB8IHwgfCB8IHwgfCAgX18vXF9fIFwKfF98IHxffFxfX198X3wgIHxffCB8X3wgfF98XF9fX3x8X19fLwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCg=="

func ShowLabel() {
	uDec, _ := b64.URLEncoding.DecodeString(label)
	label := fmt.Sprintf(`
	%s
	version  : %s
	app-name : %s
	`, string(uDec), os.Getenv("APP_VERSION"), os.Getenv("APP_NAME"))
	println(color.Ize(color.Red, label))

}

type Env struct {
	AppHost    string `env:"HOST" validate:"required"`
	AppPort    string `env:"PORT" validate:"required"`
	SmtpPort   string `env:"CONFIG_SMTP_PORT" validate:"required"`
	SmtpHost   string `env:"CONFIG_SMTP_HOST" validate:"required"`
	Email      string `env:"CONFIG_AUTH_EMAIL" validate:"required"`
	Password   string `env:"CONFIG_AUTH_PASSWORD"`
	SenderName string `env:"CONFIG_SENDER_NAME" validate:"required"`
}
